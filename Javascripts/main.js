/* Author: Lyhov Anderson
   This files cotains all the functions for the address book web page 
   This is what drives the web page
*/


// Default list that will display on window onload
var defaultList = [["Lyhov", "651-233-9175", "Anders86@augsburg.edu"],
                  ["Devin", "456-789-0123", "Devin@email.com"],
                  ["Taylor", "123-456-7890", "Taylor@email.com"],
                   ["Kari", "147-852-3690", "Kari@email.com"]];

// Buttons and forms to be used in the functions
var ourList = document.getElementById("list-items");
var listItems = document.getElementsByTagName("li");
var inputList = document.getElementsByTagName("input");
var addBtn = document.getElementById("btnAdd");
var submitBtn = document.getElementById("btnSubmit");
var cancelBtn = document.getElementById("btnCancel");
var editBtn = document.getElementById("btnEdit");
var form = document.getElementById("inputForm");
var infoDisplay = document.getElementsByTagName("p");
// Initial case for adding and editing, functions will be called accordingly
var adding = false;
var editing = false;

var spotClicked = -1;

// Update defaultList, when this function is called
function updateList() {
    for (var i=0; i<listItems.length; i++){
        listItems[i].innerHTML = defaultList[i][0];
    }
}
// Window onload layout
function init(){
    updateList();
    form.style.visibility = 'hidden';
    submitBtn.style.visibility = 'hidden';
    cancelBtn.style.visibility = 'hidden';
    editBtn.style.visibility = 'hidden';
}
window.onload = init();


// Index of a node in list item
function index(el){
    for(var i=0; i<listItems.length; i++){
        if(listItems[i]===el){
            return i;
        }
    }
   return -1;
}

// Hilight selected item from the list
ourList.addEventListener("click", activateItem);
function activateItem(e){
    // Remove "active" class from all of them first
    if(e.target.nodeName === "LI"){
        for(i=0; i<e.target.parentNode.children.length; i++){
            e.target.parentNode.children[i].classList.remove("active");
        }
        // Class "active" is in CSS folder/ bootstrap.css 
        e.target.classList.add("active");
        // Display deatails for selected item
        nameClicked(e.target);
    }
}

// Display function for selected item
function nameClicked(e){
    if(!adding && !editing){
        spotClicked = index(e);
        form.style.visibility = 'visible';        
        editBtn.style.visibility = 'visible';
        submitBtn.style.visibility = 'hidden';
        cancelBtn.style.visibility = 'hidden';      
        
        for(var j=0; j<inputList.length; j++){
            infoDisplay[j].innerHTML = defaultList[index(e)][j];
            // Hide input list
            inputList[j].style.visibility = 'hidden';
            // Display selected node details
            infoDisplay[j].style.visibility = 'visible';
        }
    }
    else{
        alert("Please submit or cancel");
    }
    // Test if spotCliced works, will return index or selected item
    //console.log(spotClicked);
}

// Function for edit button
editBtn.addEventListener("click", edit);
function edit(){    
    if(spotClicked != -1){
        editing = true;
        editBtn.style.visibility = 'hidden';
        submitBtn.style.visibility = 'visible';
        cancelBtn.style.visibility = 'visible';
        for(var i=0; i<inputList.length; i++){
            inputList[i].value = infoDisplay[i].innerHTML;
            inputList[i].style.visibility = 'visible';
            infoDisplay[i].style.visibility ='hidden';
        }
    }
}

/* Function for submit button   
   Fnction will be called accordingly to different mode: 
   Adding new item or editing existing item
*/
submitBtn.addEventListener("click", save);
function save(){
    // Make sure all fields are filled
    if(inputList[0].value != "" &&
       inputList[1].value != "" &&
       inputList[2].value != "")
    {
        if(editing){
            for(var i=0; i<inputList.length; i++){
                inputList[i].style.visibility = 'hidden';
                defaultList[spotClicked][i] = inputList[i].value;
                infoDisplay[i].innerHTML = inputList[i].value;                
                infoDisplay[i].style.visibility = 'visible';
            }
            
            cancelBtn.style.visibility = 'hidden';
            submitBtn.style.visibility = 'hidden';
            editBtn.style.visibility = 'visible';
            addBtn.style.visibility = 'visible';
            
        editing = false;
        adding = false;
        updateList();
        }
        
        if(adding){
            defaultList.push([inputList[0].value,
                              inputList[1].value,
                              inputList[2].value]);
            var newLi = document.createElement("li");
            ourList.appendChild(newLi);
            addBtn.style.visibility = 'visible';
            editBtn.style.visibility = 'visible';
            submitBtn.style.visibility = 'hidden';
            cancelBtn.style.visibility = 'hidden';
            
            for(var i=0; i<inputList.length; i++){
                infoDisplay[i].innerHTML = defaultList[index(newLi)][i];
                inputList[i].style.visibility = 'hidden';
                infoDisplay[i].style.visibility = 'visible';
            }
            
            newLi.innerHTML = inputList[0].value;
            newLi.onclick = function(){
                spotClicked = index(this);
                for(var j=0; j<inputList.length; j++){
                    infoDisplay[j].innerHTML = defaultList[index(newLi)][j];
                    inputList[j].style.visibility = 'hidden';
                    infoDisplay[j].style.visibility = 'visible';
                }
            };
        }
        
        editing = false;
        adding = false;
        updateList();
    }
    else {
        alert("All fields required");
    }
}

// Function for cancel button
cancelBtn.addEventListener("click", cancel);
function cancel(){
    for(var i=0; i<inputList.length; i++){
        inputList[i].style.visibility = 'hidden';
        infoDisplay[i].style.visibility = 'visible';
    }
    
    if(spotClicked != -1){
        editBtn.style.visibility = 'visible';
    }
    addBtn.style.visibility = 'visible';
    submitBtn.style.visibility = 'hidden';
    cancelBtn.style.visibility = 'hidden';
    
    editing = false;
    adding = false;
}


// Function for add button
addBtn.addEventListener("click", showForm);
function showForm(){
    removeHighlight();
    adding = true;
    addBtn.style.visibility = 'hidden';
    form.style.visibility = 'visible';
    submitBtn.style.visibility = 'visible';
    cancelBtn.style.visibility = 'visible';
    editBtn.style.visibility = 'hidden';
    
    for(var i=0; i<inputList.length; i++){
        inputList[i].style.visibility = 'visible';
        inputList[i].value="";
        infoDisplay[i].style.visibility = 'hidden';
    }    
}

// Remove highlight from the list
function removeHighlight(){
    for(var i=0; i<listItems.length; i++){
            listItems[i].classList.remove("active");   
    }
}
