This is a web page of a simple address book. The framework of this page is Bootstrap and HTML with CSS.
New data will not be stored on the hard-drive. Javascript handles all functions for this page.
The initial information is hard coded into an array of Javascript; and It is loaded when opening the page.

Feature of this webpage: view, add, and edit contacts information. 

To run the page please download/fork Addressbook folder and run index.html in your favorite browser, which is Google Chrome.


